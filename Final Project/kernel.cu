#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cuda.h>
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdint.h>
#include <Windows.h>
#include <iostream>

using namespace std;

#define SIZE 1024
#define BLOCK_SIZE 1

char *dataFile = "sub040.data";
char *leftAudio = "01.L.pcm";
char *rightAudio = "01.R.pcm";
char *outputHRTF = "output_HRTF.pcm";


/*	Struct for Data Input File	*/
struct Data{
	float angle;
	float elevation;
	char side;
	float h[200];
};

/** Use to init the clock */
#define TIMER_INIT \
    LARGE_INTEGER frequency; \
    LARGE_INTEGER t1,t2; \
    double elapsedTime; \
    QueryPerformanceFrequency(&frequency);


/** Use to start the performance timer */
#define TIMER_START QueryPerformanceCounter(&t1);

/** Use to stop the performance timer and output the result to the standard stream. Less verbose than \c TIMER_STOP_VERBOSE */
#define TIMER_STOP \
    QueryPerformanceCounter(&t2); \
    elapsedTime=(float)(t2.QuadPart-t1.QuadPart)/frequency.QuadPart; \
    std::wcout<<(elapsedTime)<<L" sec"<<endl;



__global__ void FIRAdd2(Data *d, float *x, float *y, int sizeOfFile, int side, int block) {
	
	int count = 0;
	float *h;
	h = d[side].h;	// h data needed for the FIR Loop

	/* Go through file */
	for(int num = threadIdx.x + (1024*+blockIdx.x); num < sizeOfFile; num+= (1024*block)) {
		/* Change Direction of Sound every so often */
		if(count == (200 / block))
			h = d[30 + side].h;
		else if(count == (400 / block)) {
			h = d[0 + side].h;
			count = 0;
		}
		if(num > 200) {
			float sum = 0;
			/* FIR Loop */
			for(int i = 0; i < 200; ++i)
				sum += h[199 - i] * x[i - 199 + (num)];
			y[num] = sum;
		}
		else
			y[num] = 0;
		count++;
	}
}

/*
	Reads in the Data file and puts it in a data Struct
*/
void read_data(struct Data *d, int size) {
	FILE* fp = fopen(dataFile, "r");
	if(fp == NULL) {
		fprintf(stderr, "Data file did not open. Now exiting.\n");
		exit(-1);
	}

	bool valid = false;
	int j = 0;
	// Go through file
	while(fscanf(fp, "%f %f %c", &d[j].angle, &d[j].elevation, &d[j].side) != EOF) {
		// I am only picking coordinates at elevation 0.
		if(d[j].elevation == 0)
			valid = true;
		// Grab the 200 floats
		for(int i = 0; i < 200; i++) {
			int num = fscanf(fp, "%f", &d[j].h[i]);
			if(valid == false)
				d[j].h[i] = 0;
		}
		// Only go to next array when we have an elevation of 0
		if(valid == true)
			j++;
		valid = false;
	}
	fclose(fp);
}

int16_t* combine_ints(int16_t *left, int16_t *right, int fileSize) {
	int16_t *combined = new int16_t[fileSize * 2];

	for(int i = 0; i < fileSize; i++) {
		combined[i*2] = left[i];
		combined[i*2 + 1] = right[i];
	}
	return combined;
}

/* getFileSize method */
/* Simply takes in a file name and determines the fileSize */
int get_file_size(char *fileName) {
	FILE *inputFile = fopen(fileName, "r");
	if(inputFile == NULL) {
		fprintf(stderr, "Could not open the input file\n");
		exit(-1);
	}

	fseek(inputFile, 0, SEEK_END);		// Go to the end of file
	int fileSize = ftell(inputFile);	// Get the file size
	fclose(inputFile);
	return fileSize;					// Return File Size
}

/* Get Audio Data */
int16_t* create_audio_array(char *fileName) {
	int fileSize = get_file_size(fileName) / sizeof(int16_t);
	FILE *inputFile = fopen(fileName, "rb");
	if(inputFile == NULL) {
		fprintf(stderr, "Could not open file %s. Exiting.", fileName);
		exit(-1);
	}

	int16_t *input_array = new int16_t[fileSize];
	int read = fread(input_array, sizeof(int16_t), fileSize, inputFile);
	if(read != fileSize) {
		fprintf(stderr, "Read is not the same as fileSize\n");
		fprintf(stderr, "Read = %i | fileSize = %i\n", read, fileSize);
		exit(-1);
	}
	
	fclose(inputFile);
	return input_array;
}

/*
	This function takes in an int16_t array of int size, makes a 
	float array of int size, aligned to 16 bytes, and then converts
	the input from int16_t to float
*/
float* int_to_float(int16_t *input, int size) {
	float* output = (float*) malloc(size*sizeof(float));
	
	if(output == NULL) {
		printf("Could not allocate memory at intToFloat\n");
		exit (-1);
	}

	for(int i = 0; i < size; i++) {
		output[i] = (float) input[i];
	}

	return output;
}

/* Convert Float to Int */
int16_t* float_to_int(float* input, int size) {
	int16_t* output = new int16_t[size];
	if(output == NULL) {
		printf("Could not allocate memory at floatToInt\n");
		exit (-1);
	}

	for(int i = 0; i < size; i++) {
		// In case of overflow
		if(input[i] > 32767.0)
			input[i] = 32767.0;
		// In case of underflow
		else if(input[i] < -32768.0)
			input[i] = -32768.0;
		output[i] = (int16_t) input[i];
	}
	return output;
}


void HRTF_demo2(Data *d) {
	printf("starting\n");
	TIMER_INIT;
	
	// Get File Size
	int fileSize = get_file_size(rightAudio) / sizeof(int16_t);
	
	// Get int array
	int16_t *input = create_audio_array(rightAudio);
	
	// put int array in float array
	float *x_input = int_to_float(input, fileSize);

	// Create Float arrays for CUDA
	float *y_left_float = new float[fileSize];
	float *y_right_float = new float[fileSize];
	float *d_x_input, *d_y_left, *d_y_right;
	struct Data *d_d;
	// Allocate Cuda memory
	printf("Doing memory allocation ... ");
	TIMER_START;
	cudaMalloc(&d_d, 100*sizeof(Data));
	cudaMalloc(&d_x_input, fileSize*sizeof(float));
	cudaMalloc(&d_y_left, fileSize*sizeof(float));
	cudaMalloc(&d_y_right, fileSize*sizeof(float));

	// Copy Memory over
	cudaMemcpy(d_d, d, 100*sizeof(Data), cudaMemcpyHostToDevice);
	cudaMemcpy(d_x_input, x_input, fileSize*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(d_y_left, y_left_float, fileSize*sizeof(float), cudaMemcpyHostToDevice);
	TIMER_STOP;
	printf(" done\n");
	
	cudaEvent_t start, stop;
	float time = 0;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	// Run FIR Left
	printf("Starting left\n");
	cudaEventRecord(start, 0);
	
	TIMER_START;
	FIRAdd2<<< BLOCK_SIZE, 1024>>> (d_d, d_x_input, d_y_left, fileSize, 0, BLOCK_SIZE);

	cudaEventRecord(stop, 0);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(&time, start, stop);
	
	printf("Time = %f\n", time);
	//cout << "Time equals " << GetCounter() << endl;
	printf("Finished left\n");
	cudaEvent_t start2, stop2;
	float time2 = 0;
	cudaEventCreate(&start2);
	cudaEventCreate(&stop2);
	time = 0;
	// Copy from device to host
	
	cudaMemcpy(y_left_float, d_y_left, fileSize*sizeof(float), cudaMemcpyDeviceToHost);
	cudaFree(d_y_left);
	
	cudaMemcpy(d_y_right, y_right_float, fileSize*sizeof(float), cudaMemcpyHostToDevice);
	// RUN FIR right
	printf("Starting right\n");

	cudaEventRecord(start2, 0);
	FIRAdd2<<< BLOCK_SIZE, 1024>>> (d_d, d_x_input, d_y_right, fileSize, 1, BLOCK_SIZE);

	cudaEventRecord(stop2, 0);
	cudaEventSynchronize(stop2);
	cudaEventElapsedTime(&time2, start2, stop2);
	
	printf("Time = %f\n", time2);
	TIMER_STOP;
	printf("Finished right\n");

	cudaMemcpy(y_right_float, d_y_right, fileSize*sizeof(float), cudaMemcpyDeviceToHost);
	// Change to ints
	int16_t *left_output = float_to_int(y_left_float, fileSize);
	int16_t *right_output = float_to_int(y_right_float, fileSize);
	
	// combine
	int16_t *combined_output = combine_ints(left_output, right_output, fileSize);

	// Open output file
	FILE *outFile = fopen(outputHRTF, "wb");
	if(outFile == NULL) {
		fprintf(stderr, "Could not open combined output file\n");
		exit(-1);
	}

	// Write to output file
	fwrite(combined_output, sizeof(int16_t), fileSize*2, outFile);

	free(y_left_float);
	free(y_right_float);
	free(x_input);
	cudaFree(d_x_input);
	cudaFree(d_y_right);
	cudaFree(d_d);
	fclose(outFile);
}

int main(int argc, char **argv) {

	struct Data d[100];
	read_data(d, 100);
	//run_HRTF(d);
	HRTF_demo2(d);
	//free(d);
	return 0;
}
